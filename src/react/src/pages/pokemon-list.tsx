import React, { FunctionComponent, useState, useEffect } from 'react';
import Pokemon from '../models/pokemon';
import PokemonCard from '../components/pokemon-card';
import PokemonService from '../services/pokemon-service';
import { Link } from 'react-router-dom';
import PokemonSearch from '../components/pokemon-search';

const PokemonList: FunctionComponent = () => {
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    PokemonService.getPokemons()
      .then(pokemons => setPokemons(pokemons || []))
      .catch(error => {
        console.error("Failed to fetch pokemons:", error);
        setError("Failed to fetch pokemons");
      });
  }, []);

  console.log(pokemons)
  return (
    <div>
      <h1 className="center">Pokédex</h1>
      <div className="container">
        <div className="row">
          <PokemonSearch />
          {error ? (
            <p className="center">{error}</p>
          ) : (
            pokemons.length > 0 ? (
              pokemons.map(pokemon => (
                <PokemonCard key={pokemon.id} pokemon={pokemon}/>
              ))
            ) : (
              <p className="center">No pokemons found.</p>
            )
          )}
        </div>
      </div>
      <Link className="btn-floating btn-large waves-effect waves-light red z-depth-3"
        style={{position: 'fixed', bottom: '25px', right: '25px'}}
        to="/pokemon/add">
        <i className="material-icons">add</i>
      </Link>
    </div>
  );
}

export default PokemonList;
